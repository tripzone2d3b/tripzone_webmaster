<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "penginapan".
 *
 * @property integer $ID
 * @property string $Nama_Inap
 * @property string $Alamat
 * @property integer $ID_kota_wisata
 * @property integer $rating
 * @property string $deskripsi
 * @property double $latitude
 * @property double $longitude
 */
class Penginapan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'penginapan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama_Inap', 'Alamat', 'ID_kota_wisata', 'rating', 'deskripsi', 'latitude', 'longitude'], 'required'],
            [['Alamat', 'deskripsi'], 'string'],
            [['ID_kota_wisata', 'rating'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['Nama_Inap'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nama_Inap' => 'Nama  Inap',
            'Alamat' => 'Alamat',
            'ID_kota_wisata' => 'Id Kota Wisata',
            'rating' => 'Rating',
            'deskripsi' => 'Deskripsi',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }
}
