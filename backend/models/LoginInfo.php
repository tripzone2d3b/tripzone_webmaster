<?php 
namespace backend\models;


use yii\base\Model;

class LoginInfo extends Model
{
    public $ID;
    public $Nama;
    public $Password;

    public function rules()
    {
        return
            [
                [['id', 'username', 'password_hash'], 'required'],
                [['id'], 'integer'],
                [['username', 'password_hash'], 'trim'],
                [['username'], 'exist', 'targetClass' => '\backend\models\Admin'],
            ];
    }

    public function importUser(Users $user)
    {
        $this->ID = $user->ID;
        $this->Nama = $user->Nama;
        $this->Password = $user->Password;
        return $this;
    }
}
 ?>