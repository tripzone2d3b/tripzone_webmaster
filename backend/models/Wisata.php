<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "wisata".
 *
 * @property integer $ID
 * @property string $Nama
 * @property string $Alamat
 * @property integer $ID_Jenis_wisata
 * @property integer $ID_kota_wisata
 * @property integer $Rating
 * @property string $Dskripsi
 *
 * @property History[] $histories
 * @property JenisWisata $iDJenisWisata
 * @property KotaWisata $iDKotaWisata
 */
class Wisata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wisata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama', 'Alamat', 'ID_Jenis_wisata', 'ID_kota_wisata', 'Rating', 'Dskripsi'], 'required'],
            [['Alamat', 'Dskripsi'], 'string'],
            [['ID_Jenis_wisata', 'ID_kota_wisata', 'Rating'], 'integer'],
            [['Nama'], 'string', 'max' => 100],
            [['ID_Jenis_wisata'], 'exist', 'skipOnError' => true, 'targetClass' => JenisWisata::className(), 'targetAttribute' => ['ID_Jenis_wisata' => 'ID']],
            [['ID_kota_wisata'], 'exist', 'skipOnError' => true, 'targetClass' => KotaWisata::className(), 'targetAttribute' => ['ID_kota_wisata' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nama' => 'Nama',
            'Alamat' => 'Alamat',
            'ID_Jenis_wisata' => 'Id  Jenis Wisata',
            'ID_kota_wisata' => 'Id Kota Wisata',
            'Rating' => 'Rating',
            'Dskripsi' => 'Dskripsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['ID_Wisata' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDJenisWisata()
    {
        return $this->hasOne(JenisWisata::className(), ['ID' => 'ID_Jenis_wisata']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIDKotaWisata()
    {
        return $this->hasOne(KotaWisata::className(), ['ID' => 'ID_kota_wisata']);
    }
}
