<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kota_wisata".
 *
 * @property integer $id
 * @property integer $nomor
 * @property string $nama
 * @property integer $singkatan
 * @property integer $provinsi_id
 *
 * @property Wisata[] $wisatas
 */
class KotaWisata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kota_wisata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomor', 'nama', 'singkatan', 'provinsi_id'], 'required'],
            [['nomor', 'singkatan', 'provinsi_id'], 'integer'],
            [['nama'], 'string', 'max' => 75],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nomor' => 'Nomor',
            'nama' => 'Nama',
            'singkatan' => 'Singkatan',
            'provinsi_id' => 'Provinsi ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWisatas()
    {
        return $this->hasMany(Wisata::className(), ['ID_kota_wisata' => 'ID']);
    }
}
