<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jenis_wisata".
 *
 * @property integer $ID
 * @property string $Nama
 *
 * @property Wisata[] $wisatas
 */
class JenisWisata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_wisata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama'], 'required'],
            [['Nama'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWisatas()
    {
        return $this->hasMany(Wisata::className(), ['ID_Jenis_wisata' => 'ID']);
    }
}
