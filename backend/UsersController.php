<?php

namespace backend\controllers;

use Yii;
use backend\models\Users;
use backend\models\LoginInfo;
use backend\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        Yii::$app->request->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Users();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->ID]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    public function actionCreateusers()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $student = new Users();
        $student->scenario = Users::SCENARIO_CREATE;
        $student->attributes = Yii::$app->request->post();

        if($student->validate()) {
            $student['ID']=$student->attributes['ID'];
            $student['Nama']=$student->attributes['Nama'];
            //$student['Nama_panggilan']=$student->attributes['Nama_panggilan'];
            $student['Alamat']=$student->attributes['Alamat'];
            $student['Telepon']=$student->attributes['Telepon'];
            $student['Email']=$student->attributes['Email'];
            $student['Password']=($student->attributes['Password']);
            $student->save();
            return array('status' => true, 'data'=> 'Student record is successfully updated');
        } else {
            return array('status'=>false,'data'=>$student->getErrors());
        }

    }
    public function actionUpdateusers()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $attributes = Yii::$app->request->post();
        $student = User::find()->where(['ID' => $attributes['ID'] ])->one();
        if(count($student) > 0 ) {
            $student->attributes = Yii::$app->request->post();
            $student['Nama']=$student->attributes['Nama'];
            //$student['Nama_panggilan']=$student->attributes['Nama_panggilan'];
            $student['Alamat']=$student->attributes['Alamat'];
            $student['Telepon']=$student->attributes['Telepon'];
            $student['Email']=$student->attributes['Email'];
            $student['Password']=$student->attributes['Password'];
            $student->save();
            return array('status' => true, 'message'=> 'Users record is updated successfully');
        } else {

            return array('status'=>false,'message'=> 'No Users Found');
        }
    }
    public function actionLogin(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $student=new Users();
        $Nama= Yii::$app->request->post('Nama','');
        $Password=(Yii::$app->request->post('Password',''));
        $data=Users::findOne(['Nama'=>$Nama, 'Password'=>$Password]);

        if (is_null($data)) {
            return [
                'status' =>false,
                'message' =>'Nama atau Password Salah',
            ];
        }else{
            $student->importUser($data);
            // $data->updated_at = date('Y-m-d H:i:s');
            // $data->password_reset_token = ($data->password_reset_token == '' || $data->password_reset_token == null) ? base64_encode( $student->username.':'.$student->password_hash ) : $data->password_reset_token;
            $data->save();
            return array_merge([
                'status' => true,
                //'role'=>$data->role->name,
                'message' => 'Login berhasil.',
                //'access_token' => $data->password_reset_token,
                //'Nama' => $data->Nama,
            ], $student->toArray());
        }
    }
    public function actionGetusers()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $student = Users::find()->all();
        if(count($student) > 0 ) {
            return array('status' => true, 'data'=> $student);
        } else             return array('status'=>false,'data'=> 'No Users Found');
        
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    
    public function actionDeleteusers()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $attributes = Yii::$app->request->post();
        $student = Users::find()->where(['ID' => $attributes['ID'] ])->one();
        if(count($student) > 0 ) {
            $student->delete();
            return array('status' => true, 'data'=> 'User record is successfully deleted');
        } else {
            return array('status'=>false,'data'=> 'No Student Found');
        }
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
