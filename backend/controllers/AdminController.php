<?php

namespace backend\controllers;
use Yii;
use yii\web\Response;
use backend\models\Admin;
class AdminController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        Yii::$app->request->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreateadmin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $student = new Admin();
        $student->scenario = Admin::SCENARIO_CREATE;
        $student->attributes = Yii::$app->request->post();

        if($student->validate()) {
            $student['id']=$student->attributes['id'];
            $student['username']=$student->attributes['username'];
            $student['email']=$student->attributes['email'];
            $student['password_hash']=$student->attributes['password_hash'];
            $student->save();
            return array('status' => true, 'data'=> 'Student record is successfully updated');
        } else {
            return array('status'=>false,'data'=>$student->getErrors());
        }

    }
    public function actionLogin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $student = new Admin();
        $username = Yii::$app->request->post('username' . '');
        $password_hash = sha1(Yii::$app->request->post('password_hash', ''));
        $data = Admin::findOne(['username' => $username, 'password_hash' => $password_hash]);

        if (is_null($data)) {
            return [
                'status' => false,
                'message' => 'Username atau Password Salah',
            ];
        }else{
            $student->importUser($data);
            // $data->updated_at = date('Y-m-d H:i:s');
            // $data->password_reset_token = ($data->password_reset_token == '' || $data->password_reset_token == null) ? base64_encode( $student->username.':'.$student->password_hash ) : $data->password_reset_token;
            $data->save();
            return array_merge([
                'status' => true,
                //'role'=>$data->role->name,
                //'message' => 'Login berhasil.',
                //'access_token' => $data->password_reset_token,
                //'Nama' => $data->Nama,
            ], $student->toArray());
        }
    }
    public function actionGetadmin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $student = Admin::find()->all();
        if(count($student) > 0 ) {
            return array('status' => true, 'data'=> $student);
        } else {
            return array('status'=>false,'data'=> 'No Student Found');
        }
    }

    public function actionUpdateadmin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $attributes = Yii::$app->request->post();
        $student = Admin::find()->where(['id' => $attributes['id'] ])->one();
        if(count($student) > 0 ) {
            $student->attributes = Yii::$app->request->post();
            $student['username']=$student->attributes['username'];
            $student['email']=$student->attributes['email'];
            $student['password_hash']=$student->attributes['password_hash'];
            $student->save();
            return array('status' => true, 'data'=> 'Student record is updated successfully');
        } else {

            return array('status'=>false,'data'=> 'No Student Found');
        }
    }

    public function actionDeleteadmin()
    {
        Yii::$app->response->format = Response:: FORMAT_JSON;
        $attributes = Yii::$app->request->post();
        $student = Admin::find()->where(['id' => $attributes['id'] ])->one();
        if(count($student) > 0 ) {
            $student->delete();
            return array('status' => true, 'data'=> 'Student record is successfully deleted');
        } else {
            return array('status'=>false,'data'=> 'No Student Found');
        }
    }
}
