<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $ID
 * @property string $Nama
 * @property string $Alamat
 * @property string $Telepon
 * @property string $Email
 * @property string $Password
 *
 * @property History[] $histories
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const SCENARIO_CREATE = 'create';
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['ID','Nama','Alamat','Telepon','Email','Password'];
        return $scenarios;
    }
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama', 'Alamat', 'Telepon', 'Email', 'Password'], 'required'],
            [['Alamat'], 'string'],
            [['Nama'], 'string', 'max' => 50],
            [['Telepon'], 'string', 'max' => 13],
            [['Email'], 'string', 'max' => 30],
            [['Password'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nama' => 'Nama',
            'Alamat' => 'Alamat',
            'Telepon' => 'Telepon',
            'Email' => 'Email',
            'Password' => 'Password',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['ID_user' => 'ID']);
    }
    public function importUser(Users $user)
    {
        $this->ID = $user->ID;
        $this->Nama = $user->Nama;
        $this->Password = $user->Password;
        return $this;
    }
}
