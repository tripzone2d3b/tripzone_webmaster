<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
use vendor\almasaeed2010;
//use backend\web\assets\img;
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= Html::img('@web/assets/img/user2-160x160.jpg', ['class' => 'img-circle', 'alt' => 'User Image']) ?>

            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->username?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?=
        Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard',
                            'url' => ['/'], 'active' => $this->context->route == 'site/index'
                        ],
                        [
                            'label' => 'Wisata',
                            'icon' => 'fa fa-database',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Kota Wisata',
                                    'icon' => 'fa fa-database',
                                    'url' => '?r=kota-wisata/',
                    'active' => $this->context->route == 'kota-wisata/index'
                                ],
                                [
                                    'label' => 'Nama Wisata',
                                    'icon' => 'fa fa-database',
                                    'url' => '?r=wisata/',
                    'active' => $this->context->route == 'wisata/index'
                                ],
                                [
                                    'label' => 'History User',
                                    'icon' => 'fa fa-database',
                                    'url' => '?r=master1/',
                    'active' => $this->context->route == 'master1/index'
                                ],
                                [
                                    'label' => 'Jenis Wisata',
                                    'icon' => 'fa fa-database',
                                    'url' => '?r=jenis-wisata/',
				    'active' => $this->context->route == 'jenis-wisata/index'
                                ],
                                 [
                                    'label' => 'Nama Penginapan',
                                    'icon' => 'fa fa-database',
                                    'url' => '?r=penginapan/',
                    'active' => $this->context->route == 'penginapan/index'
                                ],
                                [
                                   'label' => 'Nama Kuliner',
                                   'icon' => 'fa fa-database',
                                   'url' => '?r=kuliner/',
                   'active' => $this->context->route == 'kuliner/index'
                               ]
                            ]
                        ],
                        [
                            'label' => 'Users',
                            'icon' => 'fa fa-users',
                            'url' => '#',
                            'items'=>[
                                [
                                    'label' => 'Account',
                                    'icon' => 'fa fa-database',
                                    'url' => '?r=users/',
                    'active' => $this->context->route == 'users/index'
                                ]
                            ]
                        ],
                        ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                    ],
                ]
        )
        ?>

    </section>
    <!-- /.sidebar -->
</aside>
