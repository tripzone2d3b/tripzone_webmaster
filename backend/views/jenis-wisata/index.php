<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\JenisWisataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jenis Wisata';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenis-wisata-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Jenis Wisata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Nama',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
