<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KotaWisata */

$this->title = 'Create Kota Wisata';
$this->params['breadcrumbs'][] = ['label' => 'Kota Wisatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kota-wisata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
