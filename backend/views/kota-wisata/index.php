<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KotaWisataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kota Wisata';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kota-wisata-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Kota Wisata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nomor',
            'nama',
            'singkatan',
            'provinsi_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
